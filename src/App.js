import './App.css';
import axios from 'axios';
import {useState, useEffect} from 'react';
import {Coin} from "./Coin";


function App() {
  const [coins, setCoins] =  useState([]);
  const [search, setSearch] = useState('');
  const perPage = 20;

  useEffect( ()=>{
    axios.get(`https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=${perPage}&page=1&sparkline=false`)
        .then(res =>{
          setCoins(res.data)

        }).catch(error => console.log(error) )
  },[]);

  const handleChange =(e)=> {
    setSearch(e.target.value);
  };


  const filterCoin =
      coins.filter( coin =>
          coin.name.toLowerCase()
              .includes(search.toLowerCase())
      )

  return (

      <div className="coin-app">
        <div className="coin-search">
          <h1 className="coin-text">search a currency</h1>
          <form>
            <input type="text"
                   placeholder="search"
                   className="coin-input"
                   onChange={handleChange}
            />
          </form>
        </div>

        {filterCoin.map(coin => {
          return(
              <Coin
                  key={coin.name} {...coin}
                  name={coin.name}
                  image={coin.image}
                  price={coin.current_price}
                  symbol={coin.symbol}
                  marketCap={coin.market_cap}
                  priceChange={coin.price_change_percentage_24h}
                  volume={coin.total_volume}
              />
          )
        })}

      </div>
  );
}

export default App;
